package ru.semenov.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@EqualsAndHashCode
@Entity
@Table(name = Team.TEAM_TABLE_NAME)
public class Team {
    public static final String TEAM_TABLE_NAME = "teams";
    private static final String TEAM_ID_COLUMN_NAME= "id";
    private static final String TEAM_TITLE_COLUMN_NAME= "title";
    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "system-uuid")
    @UuidGenerator
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Setter(AccessLevel.NONE)
    @Column(name = TEAM_ID_COLUMN_NAME,nullable = false,unique = true)
    private String id;
    @Column(name = TEAM_TITLE_COLUMN_NAME,nullable =false,unique = true)
    private String title;
    @OneToOne(cascade = CascadeType.ALL,mappedBy = "teamName")
    private Player player;
}
