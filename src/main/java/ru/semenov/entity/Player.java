package ru.semenov.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;
import org.springframework.data.annotation.Id;
import ru.semenov.enums.Country;
import ru.semenov.enums.Gender;

import java.math.BigInteger;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Entity
@Table(name = Player.PLAYER_TABLE_NAME)
public class Player implements Comparable<Player> {
    public static final String PLAYER_TABLE_NAME = "players";
    private static final String PLAYER_ID_COLUMN_NAME = "id";
    private static final String PLAYER_FIRST_NAME_COLUMN_NAME = "first_name";
    private static final String PLAYER_LAST_NAME_COLUMN_NAME="last_name";
    private static final String PLAYER_GENDER_COLUMN_NAME = "gender";
    private static final String PLAYER_LOCAL_DATE_COLUMN_NAME = "date_of_birth";
    private static final String PLAYER_NUMBER_OF_GOALS_COLUMN_NAME = "number_of_goals";
    @Id
    @jakarta.persistence.Id
    @UuidGenerator
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "uuid",strategy = "uuid2")
    @Setter(AccessLevel.NONE)
    @Column(name = PLAYER_ID_COLUMN_NAME,nullable = false,unique = true)
    private String id;
    @NotBlank
    @Column(name = PLAYER_FIRST_NAME_COLUMN_NAME,nullable = false)
    private String firstName;
    @NotBlank
    @Column(name = PLAYER_LAST_NAME_COLUMN_NAME,nullable = false)
    private String lastName;
    @Enumerated(EnumType.STRING)
    @Column(name = PLAYER_GENDER_COLUMN_NAME,nullable = false)
    private Gender gender;
    @Column(name = PLAYER_LOCAL_DATE_COLUMN_NAME,nullable = false)
    private LocalDate dateOfBirth;
    @OneToOne
    @JoinColumn(name = "team_id")
    private Team teamName;
    @Enumerated(EnumType.STRING)
    private Country country;
    @Max(1_000_000)
    @Column(name = PLAYER_NUMBER_OF_GOALS_COLUMN_NAME)
    private BigInteger numberOfGoals = BigInteger.ZERO;
    @Transient
    private Object lock = new Object();
    public void addToGoals(BigInteger value) {
        synchronized (lock) {
            this.numberOfGoals=this.numberOfGoals.add(value);
        }
    }
    @Override
    public int compareTo(Player player) {
        return this.firstName.compareTo(player.firstName);
    }
}