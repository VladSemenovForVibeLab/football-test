package ru.semenov.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@Data
@AllArgsConstructor
public class ExceptionBody {
    private String message;
    private String code;
    private String status;
    private String path;
    private String timestamp;
    private String traceId;
    private String requestId;
    private String statusCode;
    private String error;
}
