package ru.semenov.repository;

import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Repository;
import ru.semenov.entity.Player;

@Repository
public interface PlayerRepository extends JpaRepository<Player, String> {
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Override
    <S extends Player> S save(S entity);
}