package ru.semenov.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.semenov.dto.MessageResponse;
import ru.semenov.dto.PlayerRequest;
import ru.semenov.dto.TeamRequest;
import ru.semenov.service.ITeamService;

@RestController
@RequestMapping(TeamRestController.TEAM_REST_CONTROLLER_URI)
public class TeamRestController {
    public static final String TEAM_REST_CONTROLLER_URI = "/api/v1/teams";
    private final ITeamService teamService;

    public TeamRestController(ITeamService teamService) {
        this.teamService = teamService;
    }

    @PostMapping("/add")
    public ResponseEntity<MessageResponse> add(@RequestBody TeamRequest entity) {
        return new ResponseEntity<>(teamService.add(entity), HttpStatus.OK);
    }
}
