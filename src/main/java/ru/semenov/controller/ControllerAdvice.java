package ru.semenov.controller;

import org.hibernate.PropertyValueException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.semenov.exception.ExceptionBody;
import ru.semenov.exception.PlayerNotFoundException;
import ru.semenov.exception.TeamNotFoundException;

import java.sql.SQLIntegrityConstraintViolationException;
import java.time.LocalDateTime;

@RestControllerAdvice
public class ControllerAdvice {
    @ExceptionHandler(PropertyValueException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handlePropertyValueException(PropertyValueException e) {
        return ExceptionBody
                .builder()
                .message("Вы передали неверные параметры!!")
                .status(HttpStatus.NOT_FOUND.toString())
                .code("400")
                .path(e.getLocalizedMessage())
                .timestamp(LocalDateTime.now().toString())
                .traceId(e.getStackTrace().toString())
                .build();
    }
    @ExceptionHandler(PlayerNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handlePlayerNotFoundException(PlayerNotFoundException e) {
        return ExceptionBody
                .builder()
                .message("Ошибка -> футболист не найден!")
                .status(HttpStatus.BAD_REQUEST.toString())
                .code("400")
                .path(e.getLocalizedMessage())
                .timestamp(LocalDateTime.now().toString())
                .traceId(e.getStackTrace().toString())
                .build();
    }

    @ExceptionHandler(TeamNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleTeamNotFoundException(TeamNotFoundException e) {
        return ExceptionBody
                .builder()
                .message("Ошибка -> команда не найдена!")
                .status(HttpStatus.BAD_REQUEST.toString())
                .code("400")
                .path(e.getLocalizedMessage())
                .timestamp(LocalDateTime.now().toString())
                .traceId(e.getStackTrace().toString())
                .build();
    }


    @ExceptionHandler(IllegalStateException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleIllegalState(IllegalStateException e) {
        return ExceptionBody
                .builder()
                .message("Illegal state exception!")
                .status(HttpStatus.BAD_REQUEST.toString())
                .code("400")
                .path(e.getLocalizedMessage())
                .timestamp(LocalDateTime.now().toString())
                .traceId(e.getStackTrace().toString())
                .build();
    }

    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleSQLIntegrityConstraintViolation(SQLIntegrityConstraintViolationException e) {
        return ExceptionBody
                .builder()
                .message("SQL ошибки!")
                .status(HttpStatus.BAD_REQUEST.toString())
                .code("400")
                .path(e.getLocalizedMessage())
                .timestamp(LocalDateTime.now().toString())
                .traceId(e.getStackTrace().toString())
                .build();
    }
}

