package ru.semenov.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.semenov.dto.GoalRequest;
import ru.semenov.dto.MessageResponse;
import ru.semenov.dto.PlayerRequest;
import ru.semenov.dto.PlayerResponse;
import ru.semenov.service.IPlayerService;

import java.math.BigInteger;
import java.util.List;

@RestController
@RequestMapping(PlayerRestController.PLAYER_REST_CONTROLLER_URI)
public class PlayerRestController {
    public static final String PLAYER_REST_CONTROLLER_URI = "/api/v1/players";
    private final IPlayerService playerService;

    public PlayerRestController(IPlayerService playerService) {
        this.playerService = playerService;
    }
    @GetMapping
    public ResponseEntity<List<PlayerResponse>> getPlayers(){
        return new ResponseEntity<>(playerService.getPlayers(),HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<PlayerResponse> getPlayer(@PathVariable("id") String id) {
        return new ResponseEntity<>(playerService.getPlayer(id), HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<MessageResponse> addPlayer(@RequestBody PlayerRequest entity) {
        return new ResponseEntity<>(playerService.addPlayer(entity), HttpStatus.OK);
    }

    @PatchMapping("/update/{id}")
    public ResponseEntity<PlayerResponse> updatePlayer(@PathVariable String id,
                                                       @RequestBody PlayerRequest playerRequest){
        return new ResponseEntity<>(playerService.updatePlayer(id,playerRequest),HttpStatus.OK);
    }
    @DeleteMapping("/remove/{id}")
    public ResponseEntity<MessageResponse> removePlayer(@PathVariable String id){
        return new ResponseEntity<>(playerService.deletePlayer(id),HttpStatus.OK);
    }

    @PatchMapping("/goal/{id}")
    public ResponseEntity<MessageResponse> addGoal(@PathVariable String id,
                                                  @RequestBody GoalRequest entity){
        synchronized (this){
            return new ResponseEntity<>(playerService.addGoal(id,entity),HttpStatus.OK);
        }
    }
}
