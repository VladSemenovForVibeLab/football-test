package ru.semenov.util;

import org.springframework.stereotype.Component;
import ru.semenov.dto.PlayerRequest;
import ru.semenov.entity.Player;
import ru.semenov.exception.TeamNotFoundException;
import ru.semenov.repository.TeamRepository;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PlayerRequestDtoUtil implements EntityDtoUtil<Player, PlayerRequest> {
    private final TeamRepository teamRepository;

    public PlayerRequestDtoUtil(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @Override
    public Player toEntity(PlayerRequest dto) {
        return Player
                .builder()
                .firstName(dto.getFirstName())
                .lastName(dto.getLastName())
                .gender(dto.getGender())
                .country(dto.getCountry())
                .dateOfBirth(dto.getDateOfBirth())
                .teamName(teamRepository.findByTitle(dto.getTeamName())
                        .orElseThrow(()-> new TeamNotFoundException("Команда не была найдена!")))
                .build();
    }

    @Override
    public List<Player> toEntityList(List<PlayerRequest> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public PlayerRequest toDto(Player entity) {
        return new PlayerRequest(entity);
    }

    @Override
    public List<PlayerRequest> toDtoList(List<Player> entityList) {
        return entityList.stream().map(this::toDto).toList();
    }
}
