package ru.semenov.util;

import jakarta.persistence.Column;
import org.springframework.stereotype.Component;
import ru.semenov.dto.TeamRequest;
import ru.semenov.entity.Team;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TeamRequestDtoUtil implements EntityDtoUtil<Team, TeamRequest> {
    @Override
    public Team toEntity(TeamRequest dto) {
        return Team
                .builder()
                .title(dto.getTitle())
                .build();
    }

    @Override
    public List<Team> toEntityList(List<TeamRequest> dtoList) {
        return dtoList.stream().map(this::toEntity).toList();
    }

    @Override
    public TeamRequest toDto(Team entity) {
        return new TeamRequest(entity);
    }

    @Override
    public List<TeamRequest> toDtoList(List<Team> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}
