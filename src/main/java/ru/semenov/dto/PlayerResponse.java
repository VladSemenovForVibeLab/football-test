package ru.semenov.dto;

import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import ru.semenov.entity.Player;
import ru.semenov.entity.Team;
import ru.semenov.enums.Country;
import ru.semenov.enums.Gender;

import java.math.BigInteger;
import java.time.LocalDate;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlayerResponse extends BaseResponse{
    private String firstName;
    private String lastName;
    private Gender gender;
    private LocalDate dateOfBirth;
    private String teamName;
    private Country country;
    private BigInteger numberOfGoals;
    public PlayerResponse(Player player){
        this.firstName = player.getFirstName();
        this.lastName=player.getLastName();
        this.numberOfGoals=player.getNumberOfGoals();
        this.gender=player.getGender();
        this.dateOfBirth=player.getDateOfBirth();
        this.teamName=player.getTeamName().getTitle();
        this.country=player.getCountry();
    }
}
