package ru.semenov.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.math.BigInteger;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MessageResponse extends BaseResponse{
    private String id;
    private HttpStatus status;
    private String message;
    private LocalDateTime localDateTime;
    protected BigInteger goal;
}
