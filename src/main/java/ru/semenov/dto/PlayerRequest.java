package ru.semenov.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.semenov.entity.Player;
import ru.semenov.enums.Country;
import ru.semenov.enums.Gender;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlayerRequest {
    private String firstName;
    private String lastName;
    private Gender gender;
    private LocalDate dateOfBirth;
    private String teamName;
    private Country country;
    public PlayerRequest(Player player){
        this.firstName = player.getFirstName();
        this.lastName = player.getLastName();
        this.gender =player.getGender();
        this.dateOfBirth = player.getDateOfBirth();
        this.teamName = player.getTeamName().getTitle();
        this.country = player.getCountry();
    }
}
