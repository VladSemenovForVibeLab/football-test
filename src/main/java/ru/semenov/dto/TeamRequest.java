package ru.semenov.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.semenov.entity.Team;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TeamRequest {
    private String title;
    public TeamRequest(Team team){
        this.title = team.getTitle();
    }
}
