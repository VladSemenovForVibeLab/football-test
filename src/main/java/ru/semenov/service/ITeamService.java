package ru.semenov.service;

import ru.semenov.dto.MessageResponse;
import ru.semenov.dto.TeamRequest;
import ru.semenov.entity.Team;

public interface ITeamService {
    Team getByTitle(String teamTitle);

    MessageResponse add(TeamRequest entity);
}
