package ru.semenov.service;

import org.springframework.http.HttpStatusCode;
import ru.semenov.dto.GoalRequest;
import ru.semenov.dto.MessageResponse;
import ru.semenov.dto.PlayerRequest;
import ru.semenov.dto.PlayerResponse;

import java.util.List;

public interface IPlayerService {
    PlayerResponse getPlayer(String id);

    List<PlayerResponse> getPlayers();

    MessageResponse addPlayer(PlayerRequest entity);

    PlayerResponse updatePlayer(String id, PlayerRequest playerRequest);

    MessageResponse deletePlayer(String id);

    MessageResponse addGoal(String id, GoalRequest entity);
}
