package ru.semenov.service.impl;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.semenov.dto.MessageResponse;
import ru.semenov.dto.TeamRequest;
import ru.semenov.entity.Team;
import ru.semenov.exception.TeamNotFoundException;
import ru.semenov.repository.TeamRepository;
import ru.semenov.service.ITeamService;
import ru.semenov.util.TeamRequestDtoUtil;

import java.time.LocalDateTime;

@Service
public class TeamServiceImpl implements ITeamService {
    private final TeamRepository teamRepository;
    private final TeamRequestDtoUtil teamRequestDtoUtil;

    public TeamServiceImpl(TeamRepository teamRepository, TeamRequestDtoUtil teamRequestDtoUtil) {
        this.teamRepository = teamRepository;
        this.teamRequestDtoUtil = teamRequestDtoUtil;
    }

    @Override
    @Transactional(readOnly = true)
    public Team getByTitle(String teamTitle) {
        return teamRepository.findByTitle(teamTitle)
                .orElseThrow(()->new TeamNotFoundException("Данная команда не найдена!"));
    }

    @Override
    public MessageResponse add(TeamRequest entity) {
        teamRepository.save(teamRequestDtoUtil.toEntity(entity));
        return  MessageResponse
                .builder()
                .status(HttpStatus.OK)
                .message("Успешно добавлена команда!")
                .localDateTime(LocalDateTime.now())
                .build();
    }
}
