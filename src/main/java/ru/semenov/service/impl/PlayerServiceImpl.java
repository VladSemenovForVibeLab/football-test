package ru.semenov.service.impl;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.semenov.dto.GoalRequest;
import ru.semenov.dto.MessageResponse;
import ru.semenov.dto.PlayerRequest;
import ru.semenov.dto.PlayerResponse;
import ru.semenov.entity.Player;
import ru.semenov.exception.PlayerNotFoundException;
import ru.semenov.repository.PlayerRepository;
import ru.semenov.service.IPlayerService;
import ru.semenov.service.ITeamService;
import ru.semenov.util.PlayerRequestDtoUtil;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PlayerServiceImpl implements IPlayerService {
    private final ITeamService teamService;
    private final PlayerRepository playerRepository;
    private final PlayerRequestDtoUtil playerRequestDtoUtil;

    public PlayerServiceImpl(ITeamService teamService, PlayerRepository playerRepository, PlayerRequestDtoUtil playerRequestDtoUtil) {
        this.teamService = teamService;
        this.playerRepository = playerRepository;
        this.playerRequestDtoUtil = playerRequestDtoUtil;
    }

    @Override
    @Transactional(readOnly = true)
    public PlayerResponse getPlayer(String id) {
        return new PlayerResponse(
                playerRepository.findById(id)
                        .orElseThrow(() -> new PlayerNotFoundException("Игрок не найден!"))
        );
    }

    @Override
    @Transactional(readOnly = true)
    public List<PlayerResponse> getPlayers() {
        return playerRepository.findAll()
                .stream()
                .map(PlayerResponse::new)
                .collect(Collectors.toList()
                );
    }

    @Override
    @Transactional
    public MessageResponse addPlayer(PlayerRequest entity) {
        Player save = playerRepository.save(playerRequestDtoUtil.toEntity(entity));
        return MessageResponse
                .builder()
                .id(save.getId())
                .status(HttpStatus.OK)
                .message("Пользователь успешно добавлен!")
                .localDateTime(LocalDateTime.now())
                .build();
    }

    @Override
    @Transactional
    public PlayerResponse updatePlayer(String id, PlayerRequest playerRequest) {
        Player player = playerRepository.findById(id)
                .orElseThrow(() -> new PlayerNotFoundException("Футболист не найден!"));
        return update(player, playerRequest);
    }

    @Override
    @Transactional
    public MessageResponse deletePlayer(String id) {
        playerRepository.deleteById(id);
        return MessageResponse
                .builder()
                .message("Футболист успешно удален!")
                .status(HttpStatus.OK)
                .localDateTime(LocalDateTime.now())
                .build();
    }

    @Override
    public MessageResponse addGoal(String id, GoalRequest entity) {
        Optional<Player> player = playerRepository.findById(id);
        if (player.isPresent()) {
            Player playerIsPresent = player.get();
            synchronized (playerIsPresent) {
                if(playerIsPresent.getNumberOfGoals()==null){
                    playerIsPresent.setNumberOfGoals(BigInteger.ZERO);
                }
                playerIsPresent.addToGoals(entity.getGoal());
                playerRepository.save(playerIsPresent);
            }
            return MessageResponse
                    .builder()
                    .goal(playerIsPresent.getNumberOfGoals())
                    .status(HttpStatus.OK)
                    .message("Успешно добавлен!")
                    .localDateTime(LocalDateTime.now())
                    .build();
        }
        return MessageResponse
                .builder()
                .status(HttpStatus.BAD_REQUEST)
                .message("Такого футбалиста нет!")
                .localDateTime(LocalDateTime.now())
                .build();
    }

    private PlayerResponse update(Player player, PlayerRequest request) {
        player.setCountry(request.getCountry());
        player.setGender(request.getGender());
        player.setDateOfBirth(request.getDateOfBirth());
        player.setLastName(request.getLastName());
        player.setFirstName(request.getFirstName());
        player.setTeamName(teamService.getByTitle(request.getTeamName()));
        return new PlayerResponse(player);
    }
}
